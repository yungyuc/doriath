#!/bin/bash
DORIATH_DEVENV_SRC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ -z "$1" ]; then
  DORIATH_DEVENV_DST=env
else
  DORIATH_DEVENV_DST=$1
fi
DORIATH_DEVENV_DST=${DORIATH_DEVENV_SRC}/../../build/${DORIATH_DEVENV_DST}
echo "Create environment at ${DORIATH_DEVENV_DST}"
mkdir -p ${DORIATH_DEVENV_DST}
conda create -p ${DORIATH_DEVENV_DST}/install --no-default-packages -y python
cp -f ${DORIATH_DEVENV_SRC}/start ${DORIATH_DEVENV_DST}/start
