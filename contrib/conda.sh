#!/bin/bash
conda install -q -y cmake cython numpy pytest pip
lret=$?; if [[ $lret != 0 ]] ; then exit $lret; fi
conda install -q -y -c https://conda.anaconda.org/yungyuc scotch
lret=$?; if [[ $lret != 0 ]] ; then exit $lret; fi
